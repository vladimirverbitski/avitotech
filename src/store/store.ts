import { configureStore, createAsyncThunk, createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getItem, getItemComments, getTopStories } from '../api/api';
import { NewsItem } from '../components/NewsCard/NewsCard';
import { useDispatch } from 'react-redux';


export interface AppRootState {
    loading: boolean,
    newsList: NewsItem[],
    newsItem: NewsItem,
    newsComments: NewsItem[]
}

export const fetchNews = createAsyncThunk('news/fetchNews', async () => await getTopStories());
export const fetchNewsItem = createAsyncThunk('news/fetchNewsItem', async (id: number) => getItem(id));
export const fetchNewsComments = createAsyncThunk(
    'news/fetchNewsComments',
    async (ids: number[]) => getItemComments(ids)
);
export const fetchCommentReplies = createAsyncThunk(
    'news/fetchCommentReplies',
    async (ids: number[]) => getItemComments(ids)
);

const appSlice = createSlice({
    name: 'app',
    initialState: {
        loading: false,
        newsList: [],
        newsItem: null,
        newsComments: [] as NewsItem[],
        error: null
    },
    reducers: {
        loading: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },
        commentUpdate(state, action) {
            const { commentId, replies } = action.payload;
            state.newsComments[commentId].replies = replies;
        },
        updateNewsComments(state, action) {
            state.newsComments = {...state.newsComments, ...action.payload};
        }
    },
    extraReducers(builder) {
        builder
            .addCase(fetchNews.fulfilled, (state, action: PayloadAction<any>) => {
                state.newsList = action.payload;
            })
            .addCase(fetchNewsItem.fulfilled, (state, action: PayloadAction<any>) => {
                state.newsItem = action.payload;
            })
            .addCase(fetchNewsComments.fulfilled, (state, action: PayloadAction<any>) => {
                state.newsComments = action.payload;
            });
    }
});

export const selectAllNews = (state: AppRootState) => state.newsList;
export const selectNewsItem = (state: AppRootState) => state.newsItem;
export const selectNewsComments = (state: AppRootState) => state.newsComments;
export const selectComment = (commentId: number) => createSelector(
    selectNewsComments,
    state => Object.values(state).find((comment: NewsItem) => comment.id === commentId)
);

export const setIsLoaded = () => store.dispatch(loading(true));
export const setIsLoading = () => store.dispatch(loading(false));

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();

export const { loading, commentUpdate, updateNewsComments } = appSlice.actions;

export const store = configureStore({
    reducer: appSlice.reducer
});
