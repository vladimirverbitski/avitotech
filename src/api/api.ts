import axios from 'axios';
import { NewsItem } from '../components/NewsCard/NewsCard';
import { sortByDate } from '../utils/sort';
import { setIsLoaded, setIsLoading } from '../store/store';
import { arrayToObj, newsComment } from '../utils/news-util';


export const API_URL = 'https://hacker-news.firebaseio.com/v0';

export const getTopStories = (): Promise<NewsItem[]> => {
    setIsLoading();
    return axios.get(`${ API_URL }/topstories.json`)
        .then(res => res.data)
        .then((response) => {
            const result = response.slice(0, 100);
            const detailResult: Promise<NewsItem>[] = result.map((id: number) => axios.get(`${ API_URL }/item/${ id }.json`)
                .then(res => res.data));

            return Promise.all(detailResult)
                .then(result => {
                    setIsLoaded();
                    return sortByDate(result);
                });
        });
};

export const getItem = (id: number): Promise<NewsItem> => {
    setIsLoading();
    return axios.get(`${ API_URL }/item/${ id }.json`)
        .then(res => {
            setIsLoaded();
            return res.data;
        });
};

export const getItemComments = (ids: number[]) => {
    const comments = ids.map((id: number) => axios.get(`${ API_URL }/item/${ id }.json`)
        .then(res => res.data));
    return Promise.all(comments)
        .then(res => res.map(comment => newsComment(comment)))
        .then(res => arrayToObj(res));
};
