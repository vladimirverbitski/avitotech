import './Preloader.css';


export function Preloader() {
    return (
        <div className="preloader">
            <div className="lds-dual-ring" />
        </div>
    );
}
