import './Layout.css';
import { Link } from 'react-router-dom';


export function Layout(props: { title: string, children: any }) {
    return (
        <div className="container">
            <div className="nav-bar">
                <div className="nav-bar-title">
                    <Link to="/">
                        { props.title }
                    </Link>
                </div>
            </div>
            <div className="content">
                { props.children }
            </div>
        </div>
    );
}
