import { Route, Switch } from 'react-router';
import { NewsDetail } from '../../modules/NewsDetail/NewsDetail';
import { NewsList } from '../../modules/NewsList/NewsList';


export function AppRouter() {
    return (
        <Switch>
            <Route exact path="/" component={ NewsList }/>
            <Route exact path="/news/:id" component={ NewsDetail }/>
        </Switch>
    );
}
