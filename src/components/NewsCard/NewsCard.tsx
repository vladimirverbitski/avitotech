import { Link } from 'react-router-dom';
import { dateConverter } from '../../utils/dateConverter';
import './NewsCard.css';
import rating from '../../assets/img/icon-rating.png';


export interface NewsItem {
    by: string;
    descendants: number;
    id: number;
    kids: Array<number>;
    score: number;
    time: number;
    title: string;
    type: string;
    url: string;
    parent: number;
    text: string;
    deleted: boolean;
    replies: any[];
    path: number[];
}

export function NewsCard({ item }: { item: NewsItem }) {
    return (
        <div className="news-card">
            <div className="news-info">
                <div className="news-info-left">
                    <div className="news-info__author">{ item.by }</div>
                    <div className="news-info__date">{ dateConverter(item.time) }</div>
                    <div className="news-info-rating">
                        <img src={ rating } alt=""/>
                        { item.score }
                    </div>
                </div>
            </div>
            <div className="news-title">
                <h2>
                    <Link to={ { pathname: `/news/${ item.id }` } }>
                        { item.title }
                    </Link>
                </h2>
            </div>
        </div>
    );
}
