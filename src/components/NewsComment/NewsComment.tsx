import { useCallback, useState } from 'react';
import './NewsComment.css';
import { NewsItem } from '../NewsCard/NewsCard';
import { commentUpdate, fetchCommentReplies, selectComment, updateNewsComments, useAppDispatch } from '../../store/store';
import { useSelector } from 'react-redux';


export function NewsComment(props: { comment: NewsItem }) {
    const [showReplies, setShowReplies] = useState(false);
    const dispatch = useAppDispatch();
    const comment: any = useSelector(selectComment(props.comment.id))

    const getReplies = () => {
        if (showReplies) {
            setShowReplies(false);
        } else {
            fetchReplies();
            setShowReplies(true);
        }
    };

    const fetchReplies = useCallback(() => {
        dispatch(fetchCommentReplies(comment.kids)).then((res: any) => {         
            const replies = {
                commentId: comment.id,
                replies: res.payload
            };            
            dispatch(updateNewsComments(res.payload));
            dispatch(commentUpdate(replies));
        });
    }, [dispatch, comment.id, comment.kids]);

    return (
        <div className="comment-block">
            <div className="news-info__author">{ comment.deleted ? 'unknown' : comment.by }</div>
            { comment.deleted
                ? <div className="comment-text"><i><s>deleted</s></i></div>
                : <div className="comment-text" dangerouslySetInnerHTML={ { __html: comment.text } }/> }

            { comment?.kids?.length > 0 ?
                <button className="replies-button" onClick={ getReplies }>
                    { showReplies ? 'hide' : 'show' } replies ({ comment?.kids?.length })
                </button>
                : '' }

            { showReplies ?
                <div className="replies-container">
                    { Object.values(comment.replies).map((it: any) => <NewsComment key={ it.id } comment={ it }/>) }
                </div>
                : '' }
        </div>
    );
}
