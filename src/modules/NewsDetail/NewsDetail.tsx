import { useCallback, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { NewsItem } from '../../components/NewsCard/NewsCard';
import { NewsComment } from '../../components/NewsComment/NewsComment';
import { Preloader } from '../../components/Preloader/Preloader';
import { dateConverter } from '../../utils/dateConverter';
import commentsImg from '../../assets/img/icon-comments.png';
import refreshImg from '../../assets/img/icon-refresh.png';
import './NewsDetail.css';
import { useSelector } from 'react-redux';
import {
    AppRootState,
    fetchNewsComments,
    fetchNewsItem,
    selectNewsComments,
    selectNewsItem,
    useAppDispatch
} from '../../store/store';
import { useInterval } from '../../utils/useInterval';


export function NewsDetail() {
    const { id } = useParams<{ id: string }>();
    const isLoaded = useSelector((state: AppRootState) => state.loading);
    const item = useSelector(selectNewsItem);
    const comments = useSelector(selectNewsComments);
    const dispatch = useAppDispatch();

    const getNewsItem = useCallback((id: number): void => {
        dispatch(fetchNewsItem(Number(id))).then((res: any) => {
            if (res.payload.kids) {
                dispatch(fetchNewsComments(res.payload.kids));
            }
        });
    }, [dispatch]);

    useEffect(() => {
        getNewsItem(Number(id));
    }, [id, getNewsItem]);

    useInterval(() => {
        getNewsItem(Number(id));
    }, 60000);

    const updateReplies = () => {
        dispatch(fetchNewsComments(item.kids));
    };

    if (!isLoaded) {
        return <Preloader/>;
    } else {
        return (
            <>
                { item ?
                    <div className="news-detail-container">
                        <div className="back-button">
                            <Link to="/">
                                <button className="btn-default">Back</button>
                            </Link>
                        </div>
                        <div className="detail-info">
                            <div className="news-info__author">{ item.by }</div>
                            <div className="news-date">{ dateConverter(item.time) }</div>
                            <div className="news-info-comments">
                                <img src={ commentsImg } alt="comments"/>
                                { item.descendants }
                            </div>
                        </div>
                        <div className="news-title">
                            <h1>{ item.title }</h1>
                        </div>
                        <div className="news-link">
                            <a href={ item.url } target="_blank" rel="noopener noreferrer">{ item.url }</a>
                        </div>
                        <div className="news-comments">
                            <div className="news-comments-header">
                                <button className="update-replies" onClick={ updateReplies }>
                                    <img src={ refreshImg } alt="refresh"/>
                                </button>
                                <h3>Comments:</h3>
                            </div>
                            { comments
                                ? Object.values(comments as Object).map((comment: NewsItem) => comment.parent === item.id ? <NewsComment key={ comment.id }
                                                                                   comment={ comment }/> : '')
                                : '' }
                        </div>
                    </div>
                    : ''
                }
            </>
        );
    }
}
