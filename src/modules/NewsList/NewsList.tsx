import { useCallback, useEffect } from 'react';
import { NewsCard, NewsItem } from '../../components/NewsCard/NewsCard';
import { Preloader } from '../../components/Preloader/Preloader';
import './NewsList.css';
import { AppRootState, fetchNews, selectAllNews } from '../../store/store';
import { useDispatch, useSelector } from 'react-redux';
import { useInterval } from '../../utils/useInterval';


export function NewsList() {
    const isLoaded = useSelector((state: AppRootState) => state.loading);
    const dispatch = useDispatch();
    const newsItems = useSelector(selectAllNews);

    const getNewsList = useCallback(() => {
        dispatch(fetchNews());
    }, [dispatch]);

    useEffect(() => {
        getNewsList();
    }, [getNewsList]);

    useInterval(() => {
        getNewsList();
    }, 60000);


    if (!isLoaded) {
        return <Preloader/>;
    } else {
        return (
            <div className="news-list-container">
                <div className="action-bar">
                    <button className="btn-default" onClick={ getNewsList }>Update</button>
                </div>
                { newsItems.map((item: NewsItem) => <NewsCard key={ item.id } item={ item }/>) }
            </div>
        );
    }

}
