import 'normalize.css';
import './App.css';
import { AppRouter } from './components/AppRouter/AppRouter';
import { Layout } from './components/Layout/Layout';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store/store';


function App() {
    const title = 'Hacker News';

    return (
        <Provider store={store}>
            <Router>
                <Layout title={ title }>
                    <AppRouter/>
                </Layout>
            </Router>
        </Provider>
    );
}

export default App;
