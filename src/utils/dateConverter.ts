import { format } from 'date-fns';


export function dateConverter(timestamp: number) {
    return format(new Date(timestamp * 1000), 'dd MMM yyyy HH:mm');
}
