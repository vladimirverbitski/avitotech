import { NewsItem } from '../components/NewsCard/NewsCard';


export const newsComment = (comment: NewsItem) => {
    return {
        ...comment,
        replies: [],
        path: []
    }
}

export const arrayToObj = (comments: NewsItem[]) => {
    return comments.reduce((acc, comment) => {
        return {
            ...acc,
            [comment.id]: comment
        };
    }, {});
}
