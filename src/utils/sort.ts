import { NewsItem } from '../components/NewsCard/NewsCard';


export const sortByDate = (result: NewsItem[]) => {
    return result.sort((a: NewsItem, b: NewsItem) => {
        return new Date(b.time).valueOf() - new Date(a.time).valueOf();
    });
}
